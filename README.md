# PokémAI

Projet de recherche d'IA en Master de l'ULCO.

## Installation

```bash
conda create -y -n pokemaiEnv python=3.8
conda activate pokemaiEnv
conda install -y numpy tensorflow pandas matplotlib keras pydot graphviz scikit-learn natsort
pip install --ignore-installed --upgrade tensorflow==2.3.1
python3 main.py
```

## Développeurs

- Ernesto Artigas
- Alexandre Fovet
- Arthur Ringot

## Reporter

- Fabien Teytaud