from sklearn.model_selection import train_test_split
import pandas as pd

class DataUtility:
    """
        Nom : Constructeur
        Paramètres :
            - path, le chemin vers notre fichier de données brutes
            - encodingUtlity, utilitaire d'encodage pour le traitement de nos données
            - sc, Standard Scaler nous permettant de normaliser nos données
    """
    def __init__(self, path, encodingUtlity, sc):
        self.path = path
        self.encodingUtlity = encodingUtlity
        self.sc= sc

    """
        Nom : createCorrelationModifiedMatrix
        Description :
            On crée une matrice de corréaltion à partir de la matrice d'entrée.
            Puis on extrait la dernière colonne afin de conserver que nos données importantes.
        Paramètres :
            -data, notre base de données sous forme de matrice.
        Renvoie : une matrice de données sans la dernière colonne.
    """
    def createCorrelationModifiedMatrix(self, data):
        matrix = data.corr("spearman")
        return matrix[matrix.columns[-1]]

    """
        Nom : importData
        Description :
            Permet d'importer un fichier de données brutes et de le rendre dans un format plus lisible
        Renvoie : un ensemble de données sous la forme d'un DataFrame
    """
    def importData(self):
        # Importation de notre base de données nommée "data.csv".
        self.data = pd.read_csv(self.path)
        # Pour optimiser notre code, on pourrait stocker les clés de notre matrices (corrMatrix.keys) dans un array.
        # On pourrait donc supprimer des élèments depuis celui-ci et appeler la méthdoe data.drop une seule fois.
        corrMatrix = self.createCorrelationModifiedMatrix(self.data)
        for label in corrMatrix.keys():
            if (corrMatrix[label] > (-0.05) and corrMatrix[label] < 0.05) :
                self.data = self.data.drop(columns=[label])
        # Suppression des valeurs "total", ne servant à rien à première vue, elles peuvent impacter sur l'apprentissage du réseau de neurones.
        self.data = self.data.drop(columns=['First_pokemon_Total', 'Second_pokemon_Total'])
        # Là où il n'y a pas de valeur, on écrit no_value dans notre base de données.
        self.data.fillna("noValue", inplace=True)
        return self.data

    """
        Nom : splitDataToXAndY
        Description :
            Est utilisé exclusivement par le réseau de neurones pour le moment,
            Cette méthode sépare nos données en deux ensembles de données :
            Entrée (X) et Sortie (Y)
        Paramètres :
            -data, notre base de données sous forme de matrice.
        Renvoie : une paire d'ensemble X, Y
    """
    def splitDataToXAndY(self, data):
        X = data.iloc[:, 0:(len(data.columns)-1)].values
        Y = data.iloc[:, (len(data.columns)-1)].values
        return X, Y

    """
        Nom : encodeDataXAndY
        Description :
            Cette méthode encode deux sets de données afin de les rendre utilisable par le réseau de neurones.
        Paramètres :
            -X, set de donnée Entrée
            -Y, set de donnée Sortie
        Renvoie : une paire d'ensemble X, Y encodée
    """
    def encodeDataXAndY(self, X, Y):
        self.encodingUtlity.encodeInputAndOutput(X, Y)
        return X, Y

    """
        Nom : splitDataToTrainAndTest
        Description :
            Cette méthode sépare deux ensembles en quatres sous-ensembles.
            Pour chaque set X et Y, on retourne deux set de données servant pour l'entrainement et les tests du réseau de neurones.
        Paramètres :
            -X, set de donnée Entrée
            -Y, set de donnée Sortie
        Renvoie : 4 ensembles : xTrain, xTest, yTrain, yTest
    """
    def splitDataToTrainAndTest(self, X, Y):
        ## Separation des données en 2 sets -> Training et Test.
        # -- 0.2 -> 20% des données sont utilisés pour les tests et le reste pour le training.
        # -- 1337 -> Valeur de randomization permettant de séparer les données training/test aléatoirement.
        return train_test_split(X, Y, test_size = 0.2, random_state = 1337)

    """
        Nom : transformData
        Description :
            Cette méthode normalise deux sets de données et les renvoie.
            Le but de la normalisation est de rendre nos données toujours comprises entre -1 et 1.
        Paramètres :
            -xTrain, set de donnée dédié à l'entrainement du réseau de neurones
            -xTest, set de donnée dédié aux tests du réseau de neurones
        Renvoie : xTrain et xTest, normalisés
    """
    def transformData(self, xTrain, xTest):
        xTrain = self.sc.fit_transform(xTrain)
        xTest = self.sc.transform(xTest)
        return xTrain, xTest

    """
        Nom : splitAndEncode
        Description :
            Cette méthode sépare un fichier de données brutes en deux sets X et Y et les renvois encodés à la volée.
        Paramètres :
            -data, notre base de données sous forme de matrice.
        Renvoie : une paire d'ensemble X, Y
    """
    def splitAndEncode(self, data):
        X, Y = self.splitDataToXAndY(data)
        return self.encodeDataXAndY(X, Y)