from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import numpy as np

class EncodingUtility:
    """
        Nom : Constructeur
        Paramètres :
            - standardScaler, Standard Scaler nous permettant de normaliser nos données
    """
    def __init__(self, standardScaler):
        # Normalisation de labels.
        self.labelencoder = LabelEncoder()
        # Encoder les données sous forme d'un array.
        self.onehotencoder = OneHotEncoder()
        self.standardScaler = standardScaler

    """
        Nom : valueNeedToBeencoded
        Description :
            Verifie si une valeur a besoin d'être encodée de manière catégorique.
            Exemple : Si il s'agit d'une string ou d'un booleen représentant un type...
        Paramètres :
            -val, qui est la valeur que l'on vérifie.
        Renvoie : Booléen
            -True : La valeur doit être encodée.
            -False : La valeur ne doit pas être encodée.
    """
    def valueNeedToBeEncoded(self, val):
        if isinstance(val, str) or isinstance(val, np.bool_) or type(val) == type(True):
            return True
        else:
            return False

    """
        Nom : encodeInputAndOutput
        Description :
            Permet de réaliser un encodage automatique de deux ensembles de données (IN/OUT).
        Paramètres :
            -X, données d'entrée.
            -Y, données de sortie.
        Renvoie : Void
    """
    def encodeInputAndOutput(self, X, Y):
        # Si les données d'entrée existent...
        if X is not None:
            # Pour chaque type de donnée en entrée...
            for i in range(len(X[0])):
                # Si la première valeur de ce type a besoin d'être encodée de manière catégorique.
                # (Exemple :Si il s'agit d'une string ou d'un booleen représentant un type, etc...)
                if self.valueNeedToBeEncoded(X[:, i][0]):
                    # Encodage de toutes les valeurs correspondantes à la donnée cible.
                    X[:, i] = self.labelencoder.fit_transform(X[:, i])
            # On encode toutes les données (toutes numériques désormais) au format float_32.
            X = self.onehotencoder.fit_transform(X).toarray()
            # Suppression de données "factice".
            X = X[:, 1:]
        # Si les données de sortie existent...
        if Y is not None:
            # Si la première valeur de ce type a besoin d'être encodée de manière catégorique...
            if self.valueNeedToBeEncoded(Y[0]):
                # Encodage de toutes les valeurs correspondantes à la donnée cible.
                Y = self.labelencoder.fit_transform(Y)

    """
        Nom : createCustomSetForPrediction
        Description :
            Cette méthode permets de créer un set de données compatible avec le réseau de neurones.
        Paramètres :
            -data, notre set de données sous forme de DataFrame.
        Renvoie : un set de données pouvant être injecté directement en prédiction pour notre réseau de neurones.
    """
    def createCustomSetForPrediction(self, data):
        genitors_value = data.iloc[:, 0:(len(data.columns))].values
        self.encodeInputAndOutput(genitors_value, None)
        return self.standardScaler.transform(genitors_value)