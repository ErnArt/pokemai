import numpy as np
import pandas as pd
import random

""" Version basique de l'algorithmme évolutionnaire :
-> les variations ne respectent pas les règles du jeu pokemon.
"""
class EvolutionaryAlgorithm:
    """
        Nom : Constructeur
        Paramètres :
            - classifier, instance du réseau de neurones.
            - encodingUtility, Utilitaire d'encodage de données.
    """
    def __init__(self, classifier, encodingUtility):
        self.classifier = classifier
        self.encodingUtility = encodingUtility

    """
        Nom : compute
        Description :
            Fonction principale de la classe permettant de lancer l'algorithme évolutionnaire.
        Paramètres :
            - data, Set de données brutes pour l'algorithme
            - generationNb, Nombre de générations maximum
            - genitorsPairsNb, Nombre de couple de géniteurs à trouver (à chaque génération)
            - pCross, probabilité qu'un individu soit croisé avec un autre individu parmi les géniteurs
            - pMut, probabilité qu'un individu (parmi les géniteurs) subisse une mutation
            - pMutPerValue, probabilité qu'une mutation de caractéristique d'un individu subisssant une mutation survienne
            - fileName, nom de fichier pour les résultats.
    """
    def compute(self, data, generationNb, genitorsPairsNb, pCross, pMut, pMutPerValue, fileName):
        print("The evolutionary algorithm is running, please wait...")
        # Récupèration de la population initiale à partir de nos données de combat.
        initialPopulation = self.getPopulation(data)
        # Sauvegarde de la population initiale.
        population = initialPopulation;
        generationIndex = 0
        # Tant que le nombre de génération ne dépasse pas le nombre de génération max admis.
        while generationIndex < generationNb:
            # On récupère ici une liste de couples de géniteurs, tous choisis aléatoirement parmi la population.
            genitors = self.findGenitors(population, genitorsPairsNb)
            # On va évaluer chaque couple de géniteur afin de ne sélectionner que les meilleurs.
            bestGenitors = self.findBestGenitors(genitors)
            # Les meilleurs géniteurs subissent une variation (de type Mutation et/ou Croisement)
            # La liste retournée ci-dessous forme alors un ensemble "d'enfants"
            childrens = self.variation(bestGenitors, pCross, pMut, pMutPerValue)
            # Des combats sont ensuite organisés entre les enfants et des membres de la population intiale
            # Pour chaque combat, 
            #   Si l'enfant gagne, il prends la place de son adversaire dans la population
            #   Si l'enfant perds, le membre de la population garde sa place.
            population = self.replacement(population, childrens)
            # Ainsi, la population devient composée d'individus de plus en plus forts....
            # La "performance" de cette population finale dépend donc du nombre de générations (/nombre de remplacements effectués).
            generationIndex = generationIndex + 1
        print("Collection of the best Pokemon in progress...")
        # Les anciens pokemons ré-intégrent la population pour les tests.
        population = population.append(initialPopulation, ignore_index=True)
        population.drop_duplicates(ignore_index=True, inplace=True)
        # Triage auto des Pokemon selon leurs scores de combat : chaque pokemon affronte tous les autres.
        bestPokemons = self.findBestPokemon(population)
        # Exportation au format csv.
        self.exportBestPokemon(bestPokemons, fileName)

    """
        Nom : getPopulation
        Description :
            Fonction permettant de récupèrer l'ensemble de la population initiale de pokemon depuis des données de combats.
            Cette fonction nous permet également de définir des variables qui serviront à créer nos propres combats.
            Et enfin, elle permet de garder en mêmoire la liste des types 1 et 2 de pokemons.
        Paramètres :
            - data, Set de données brutes pour la recherche de pokemons
        Renvoie : La liste des pokemons
    """
    def getPopulation(self, data):
        # On ne va s'interesser qu'aux premiers pokemons de chaque combat, on met donc en place un splitter.
        self.splitter = int(len(data.columns)/2)
        # On utiliser le splitter pour récupèrer que les premières colonnes de chaque combat
        pokemons = data.iloc[:,:self.splitter]
        # On supprime les pokemons présents plusieurs fois dans la même liste
        pokemons = pokemons.drop_duplicates()
        # Récupèration d'une liste regroupant tout les types 1 de pokemon
        self.pokemonType1List = data.drop_duplicates(subset=['First_pokemon_Type_1']).iloc[:, 0].values
        # Récupèration d'une liste regroupant tout les types 2 de pokemon
        self.pokemonType2List = data.drop_duplicates(subset=['First_pokemon_Type_2']).iloc[:, 1].values
        # Récupèration de la liste des colonnes des données de combat (on n'utilise pas la dernière colonne car celle ci concerne le gagnant du combat).
        self.fightColumnsList = data.columns[0:-1]
        # Récupèration de la liste des colonnes de population
        self.populationColumnsList = pokemons.columns
        return pokemons

    """
        Nom : findGenitors
        Description :
            Fonction permettant de trouver des couples de géniteurs aléatoires parmi une population.
        Paramètres :
            - population, Liste de pokemons représentant la population
            - genitorsPairsNb, Nombre de couples de géniteurs à trouver
        Renvoie : Une liste de couples de géniteurs
    """
    def findGenitors(self, population, genitorsPairsNb):
        genitorsPairs = []
        # Pour chaque géniteur à trouver...
        for i in range(genitorsPairsNb):
            # On recherche deux index aléatoires différents entre eux, borné par la taille de notre population.
            index1, index2 = self.findCoupleOfUniqueIndex(population)
            # Création d'un nouveau couple de géniteur.
            genitorPair  = np.concatenate((np.array(population.iloc[index1, :]), np.array(population.iloc[index2, :])))
            # Ajout de ce couple à la listes des couples de géniteurs.
            genitorsPairs.append(genitorPair)
        # On retourne un DataFrame contenant la liste des couples de géniteurs.
        return pd.DataFrame(genitorsPairs, columns=self.fightColumnsList)

    """
        Nom : findBestGenitors
        Description :
            Fonction permettant de faire combattre (/évaluer) chaque couple de géniteur donné en paramètre.
            Et qui retourne la liste des meilleurs.
        Paramètres :
            - genitors, Liste des couples de géniteurs à évaluer
        Renvoie : Une liste des meilleurs géniteurs
    """
    def findBestGenitors(self, genitors):
        # Liste vide nous permettant de stocker les meilleurs géniteurs.
        bestGenitors = []
        # Transformation d'une liste de couples de géniteurs en un ensemble de données pouvant être utilisé en prédiction pour notre réseau de neurones.
        genitorsList =  self.encodingUtility.createCustomSetForPrediction(genitors)
        # Récupèration des prédictions pour chaque combat.
        fightPrediction = pd.DataFrame(self.classifier.predict(genitorsList))
        # Pour chaque combat effectué...
        for i in range(len(fightPrediction)):
            # Si le geniteur 1 du couple i a plus de chances de gagner face au géniteur 2 alors...
            if(fightPrediction.iloc[i,0] > 0.5):
                # On récupère le geniteur 1 pour le placer dans la liste des meilleurs géniteurs.
                bestGenitors.append(genitors.iloc[i,:self.splitter].values)
            else: # Sinon...
                # On récupère le geniteur 2 pour le placer dans la liste des meilleurs géniteurs.
                bestGenitors.append(genitors.iloc[i,self.splitter:].values)
        # On retourne un DataFrame contenant la liste des meilleurs géniteurs.
        return pd.DataFrame(bestGenitors, columns=self.populationColumnsList)

    """
        Nom : variation
        Description :
            Fonction permettant de faire varier un ensemble d'individus.
            L'ensemble des indivus résultant devient un ensemble d'enfants.
        Paramètres :
            - individuals, Liste des individus apte à subir une mutation et/ou un croisement.
            - pCross, probabilité qu'un individu soit croisé avec un autre individu parmi la liste des individus
            - pMut, probabilité qu'un individu subisse une mutation
            - pMutPerValue, probabilité qu'une mutation de caractéristique d'un individu survienne
        Renvoie : Une liste d'enfants
    """
    def variation(self, individuals, pCross, pMut, pMutPerValue):
        # Création d'une liste indvidualsWithMutations
        # Elle contiendra directement une seconde liste, issue de notre liste d'individus initiale, avec un appel automatique à la fonction "mutation" (gestion des mutations) pour chaque ligne !
        indvidualsWithMutations = individuals.apply(self.mutation, axis=1, args=(pMut, pMutPerValue))
        # La liste va ensuite passer par une seconde fonction permettant de gérer les croisements d'individus.
        childrensList = self.crossover(indvidualsWithMutations, pCross)
        # On retourne finalement la liste des enfants obtenus (après mutation et croisement).
        return childrensList

    """
        Nom : mutation
        Description :
            Fonction permettant de vérifier si un individu va subir une mutation (selon une certaine probabilité).
            Si oui, la fonction retournera l'individu modifié.
        Paramètres :
            - individual, individu apte à subir une mutation.
            - pMut, probabilité qu'un individu subisse une mutation
            - pMutPerValue, probabilité qu'une mutation de caractéristique d'un individu survienne.
        Renvoie : Un individu, modifié ou non selon la probabilité de mutation
    """
    def mutation(self, individual, pMut, pMutPerValue):
        # Probablité qu'un individu subisse une mutation vérifiée ici.
        if random.random() < pMut :
            # Sauvegarde du total de pokemon avant mutation complète.
            baseTotal = individual[2] + individual[3] + individual[4] + individual[5] + individual[6] + individual[7]
            # Pour chaque statistique à changer
            for i in range(len(individual)):
                # Sauvegarde du total avant variation de stat.
                currentTotal = individual[2] + individual[3] + individual[4] + individual[5] + individual[6] + individual[7]
                # Variation de stat.
                individual[i] = self.mutate(i, individual[i], pMutPerValue, baseTotal, currentTotal)
        # On retourne le pokemon muté.
        return individual

    """
        Nom : mutate
        Description :
            Fonction permettant d'appliquer une mutation ou non, selon une certaine probabilité, sur une caractéristique d'individu.
        Paramètres :
            - feature, caractéristique qui va éventuellement subir une mutation.
            - pMutPerValue, probabilité qu'une mutation de caractéristique survienne.
        Renvoie : Une caractéristique, modifié ou non selon la probabilité de mutation.
    """
    def mutate(self, index, feature, pMutPerValue, baseTotal, currentTotal):
        # Probablité qu'une caractéristique subisse une mutation vérifiée ici.
        if(random.random() < pMutPerValue):
            # Si la caractéristique est de type String...
            if isinstance(feature, str) :
                if feature in self.pokemonType1List : # Si la caractéristique est un type de pokemon 1...
                    # On retourne un nouveau type, différent du précédent.
                    feature = self.findNewRandomTypeForPokemon(feature, self.pokemonType1List);
                elif feature in self.pokemonType2List : # Si la caractéristique est un type de pokemon 2...
                    # On retourne un nouveau type.
                    feature = self.findNewRandomTypeForPokemon(feature, self.pokemonType2List);
            # Si la caractéristique est de type booléenne...
            elif isinstance(feature, np.bool_) or type(feature) == type(True):
                # On retourne la valeur booléenne inverse.
                feature = not feature
            # Sinon, la caractéristique est de type entier
            else:
                # Si jamais le total du pokemon AVANT mutation est inférieur au total de pokemon actuel...
                if(baseTotal < currentTotal):
                    # La prochaine variation sera négative pour se rapprocher du total de base.
                    feature = feature + random.randint(-5, -1)
                # Sinon le total du pokemon AVANT mutation est supérieur ou égal au total de pokemon actuel...
                else:
                    # La prochaine variation sera positive pour les mêmes raisons...
                    feature = feature + random.randint(1, 5)
                # Si jamais la caracteristique devient négative, on fait en sorte qu'elle ne le soit plus...
                while feature < 0 :
                    feature = feature + random.randint(1, 5)
                # Si jamais la caractéristique n'est pas une stat d'HP, on fixe une limite de valeur à 255.
                if(index != 2):
                    while feature > 255 :
                        feature = feature - random.randint(1, 5)
                # ... mais si la caractéristique est une stat d'HP, ça limite est fixée à 999.
                else:
                    while feature > 999 :
                        feature = feature - random.randint(1, 5)
        return feature

    """
        Nom : crossover
        Description :
            Fonction permettant parcourir chaque indvidu d'une population avec un pas à 2.
            Pour chaque couple parcouru, on tente un croisement entre les deux individus du couple, selon une probabilité.
        Paramètres :
            - individuals, liste d'individus
            - pCross, probabilité qu'un individu soit croisé avec un autre individu parmi la liste des individus
        Renvoie : Un ensemble d'individue, modifié ou non selon la probabilité de croisement.
    """
    def crossover(self, individuals, pCross):
        # Pour chaque couple d'individu...
        for i in range(0, len(individuals)-1, 2):
            # Probablité que deux individus soient croisés vérifiée ici.
            if random.random() < pCross:
                # Appel à une fonction qui se charge de retourner un couple d'individu croisé, en fournissant le couple en paramètre.
                individuals.iloc[i, :], individuals.iloc[i+1, :] = self.crossoverBetWeenTwoIndividuals(individuals.iloc[i, :], individuals.iloc[i+1, :])
        # On retourne la liste des individus.
        return individuals

    """
        Nom : crossoverBetWeenTwoIndividuals
        Description :
            Fonction permettant de réaliser un croisement entre deux individus.
        Paramètres :
            - individual1, individu 1
            - individual2, individu 2
        Renvoie : le couple d'individu croisé.
    """
    def crossoverBetWeenTwoIndividuals(self, individual1, individual2) :
        # Création de deux listes de valeurs (contenant uniquement les caractéristiques) à partir des deux individus.
        individual1Values = individual1.values
        individual2Values = individual2.values
        # Création d'un point de croisement aléatoire.
        crossoverPoint = random.randint(0, len(individual1Values))
        # On effectue le croisement de caractéristique entre les deux individus, après le point de croisement aléatoire.
        for i in range(crossoverPoint, len(individual1Values)) :
            individual1Values[i], individual2Values[i] = individual2Values[i], individual1Values[i]
        # Création de séries Pandas pour respecter le type de données en entrée.
        individual1WithCrossOver = pd.Series(individual1Values, index=individual1.index)
        individual2WithCrossOver = pd.Series(individual2Values, index=individual2.index)
        # On retourne les deux individus.
        return individual1WithCrossOver, individual2WithCrossOver

    """
        Nom : replacement
        Description :
            Fonction permettant d'effectuer un remplacement entre une population de base et des enfants.
            Ce remplacement se base sur une évaluation par combat de pokemons.

            Chaque enfant va affronter un membre de la population aléatoire (sans que deux enfants n'affronte le même).
            A l'issue de chaque combat, le gagnant possède une place dans la nouvelle population, retournée en fin de fonction

            Ainsi, si un enfant gagne face à un membre de la population existant, il prend sa place.
            A l'inverse, si le membre de la population gagne, il garde sa place et l'enfant est oublié.
        Paramètres :
            - population, ensemble de la population
            - childrens, ensemble d'enfants
        Renvoie : la nouvelle population, fruit des meilleurs en combat : population existante vs enfants.
    """
    def replacement(self, population, childrens):
        # Création d'une liste d'index de population qui permettra de connaitre quel individu a déja été pris en combat ou non.
        populationIndexList = []
        # Liste qui stockera les combats à prédire.
        fightList = []
        # Pour chaque enfant existant...
        for i in range(len(childrens)):
            # On recherche un nouvel adversaire parmi la population.
            indexPopulation = self.findUniqueIndex(population, populationIndexList)
            # On créé un ensemble de données de combat (regroupant l'enfant et le pokemon faisant parti de la population actuelle).
            newPopulationFight = np.concatenate((np.array(childrens.iloc[i, :]), np.array(population.iloc[indexPopulation, :])))
            # Ajout de cette ligne dans le tableau des combats.
            fightList.append(newPopulationFight)
            # Suppression du pokemon pris en combat de sa population.
            population = population.drop(population.index[indexPopulation])
        # La nouvelle population possédera déja tous les individus non concernés par les combats.
        newPopulation = population.values
        # Création d'un dataFrame correspondant au combats à prédire.
        fightDataFrame = pd.DataFrame(fightList, columns=self.fightColumnsList)
        # Préparation des données avant prédiction par le réseau de neurones.
        fightList = self.encodingUtility.createCustomSetForPrediction(fightDataFrame)
        # Récupèration des prédictions.
        fightPrediction = pd.DataFrame(self.classifier.predict(fightList))
        # Pour chaque prédiction de combat (et donc pour chaque couple de pokemon concerné)...
        for i in range(len(fightPrediction)):
            # Si le pokemon 1 du couple a plus de chances de gagner face au pokemon 2 alors...
            if(fightPrediction.iloc[i,0] > 0.5):
                # On ajoute à la nouvelle population le pokemon 1.
                newPopulation = np.vstack([newPopulation, fightDataFrame.iloc[i,:self.splitter].values])
            else: # Sinon...
                # On ajoute à la nouvelle population le pokemon 2.
                newPopulation = np.vstack([newPopulation, fightDataFrame.iloc[i,self.splitter:].values])
        # On retourne un DataFrame contenant la liste des gagnants : c'est la nouvelle génération.
        return pd.DataFrame(newPopulation, columns=population.columns)

    """
        Nom : findBestPokemon
        Description :
            Fonction permettant de trier une population selon leurs capacités en combat.
        Paramètres :
            - population, liste de pokemons
        Renvoie : une liste triée : le premier pokemon est celui ayant gagné le plus de combats.
    """
    def findBestPokemon(self, population):
        # Liste qui stockera les combats à prédire.
        fightList = []
        # Liste qui stockera tous les index de combat.
        indexList = []
        for i in range(len(population)):
            for j in range(i):
                # Sauvegarde des index de combat.
                indexList.append((i,j))
                # Création d'un combat à prédire.
                newPopulationFight = np.concatenate((np.array(population.iloc[i, :]), np.array(population.iloc[j, :])))
                # Ajout du combat dans un tableau.
                fightList.append(newPopulationFight)
        # Création d'un dataFrame correspondant aux combats à prédire.
        fightDataFrame = pd.DataFrame(fightList, columns=self.fightColumnsList)
        # Préparation des données avant prédiction par le réseau de neurones.
        fightList = self.encodingUtility.createCustomSetForPrediction(fightDataFrame)
        # Récupèration des prédictions.
        fightPrediction = pd.DataFrame(self.classifier.predict(fightList))
        # Ajout d'une colonne "Score" pour les Pokemon.
        population.insert(0, "Score", 0)
        # Pour chaque prédiction de combat (et donc pour chaque couple de pokemon concerné)...
        for i in range(len(fightPrediction)):
            # Si le pokemon 1 du couple a plus de chances de gagner face au pokemon 2 alors...
            if(fightPrediction.iloc[i,0] > 0.5):
                # On incremente le score du pokemon concerné.
                population.at[indexList[i][0], 'Score'] = population.iloc[indexList[i][0]]['Score'] + 1
            else: # Sinon...
                # On incremente le score de l'autre Pokemon.
                population.at[indexList[i][1], 'Score'] = population.iloc[indexList[i][1]]['Score'] + 1
        # Triage des Pokemon selon leur score : Ordre décroissant.
        population.sort_values(by=['Score'], ascending=False, inplace=True)
        return population

    """
        Nom : findCoupleOfUniqueIndex
        Description :
            Fonction permettant de récupèrer un couple d'index aléatoires différents entre eux.
            L'index aléatoire doit se baser sur une liste.
        Paramètres :
            - list, liste de données quelconque à utiliser pour trouver deux index.
        Renvoie : un couple d'indices aléatoires différents entre eux.
    """
    def findCoupleOfUniqueIndex(self, list):
        id1 = random.randint(0,len(list.index)-1)
        id2 = random.randint(0,len(list.index)-1)
        while id1 == id2 :
            id2 = random.randint(0,len(list.index)-1)
        return id1, id2

    """
        Nom : findNewRandomTypeForPokemon
        Description :
            Fonction permettant de retourner un type de pokemon aléatoire différent du type donné en paramètre.
        Paramètres :
            - currentType, le type ne pouvant être retourné par cette fonction.
            - la liste des types à utiliser pour trouver un nouveau type.
        Renvoie : un nouveau type de pokemon parmi la liste, différent du type donné en paramètre.
    """
    def findNewRandomTypeForPokemon(self, currentType, typeList):
        # On prend au hasard un nouveau type de pokemon parmi la liste.
        newType = typeList[random.randint(0, len(typeList)-1)]
        while newType == currentType : # Le nouveau type doit être différent du précédent...
            newType = typeList[random.randint(0, len(typeList)-1)]
        return newType

    """
        Nom : findUniqueIndex
        Description :
            Fonction permettant de retourner un index quelconque d'une liste, sans que celui-ci soit présent dans une seconde liste donnée en paramètre.
        Paramètres :
            - list, liste de données quelconque à utiliser pour trouver un index.
            - indexList, la liste des index à ne pas retourner.
        Renvoie : un nouveau index, non présent dans indexList.
    """
    def findUniqueIndex(self, list, indexList):
        index = random.randint(0,len(list.index)-1)
        while index in indexList:
            index = random.randint(0,len(list.index)-1)
        indexList.append(index)
        return index

    """
        Nom : exportBestPokemon
        Description :
            Fonction permettant d'exporter les meilleurs Pokemon au format csv.
            Cette même fonction va également calculer la valeur de stat total pour chaque pokemon concerné.
        Paramètres :
            - dataframe : dataframe à exporter.
            - fileName, nom de fichier pour l'exportation.
    """
    def exportBestPokemon(self, dataframe, fileName):
        print(fileName)
        # Récupèration des colonnes du dataframe.
        columnList = list(dataframe)
        # On enlève les colonnes non utilisées pour le calcul : somme des stats.
        columnList.remove("Score")
        columnList.remove("First_pokemon_Type_1")
        columnList.remove("First_pokemon_Type_2")
        columnList.remove("First_pokemon_legendary")
        # Ajout d'une nouvelle colonne.
        dataframe.insert(3, "First_pokemon_Total", 0.0)
        # Calcul des somme de stat pour chaque ligne.
        dataframe["First_pokemon_Total"] = dataframe[columnList].sum(axis=1)
        # Exportation au format csv.
        dataframe.to_csv(fileName+'.csv', index=False)