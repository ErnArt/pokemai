from classes.pokemon import extractFromLine, Pokemon, TemplatePokemon
from typing import List
import numpy as np
import pandas as pd
import random
import copy

""" Version améliorée de l'algorithmme évolutionnaire :
-> les variations respecte les règles du jeu pokemon : Implication de IV et EV.
"""
class EvolutionaryAlgorithmV2:
    """
        Nom : Constructeur
        Paramètres :
            - classifier, instance du réseau de neurones.
            - encodingUtility, Utilitaire d'encodage de données.
    """
    def __init__(self, classifier, encodingUtility):
        self.classifier = classifier
        self.encodingUtility = encodingUtility

    """
        Nom : compute
        Description :
            Fonction principale de la classe permettant de lancer l'algorithme évolutionnaire.
        Paramètres :
            - generationNb, Nombre de générations maximum
            - genitorsPairsNb, Nombre de couple de géniteurs à trouver (à chaque génération)
            - pCross, probabilité qu'un individu soit croisé avec un autre individu parmi les géniteurs
            - pMut, probabilité qu'un individu (parmi les géniteurs) subisse une mutation
            - pMutPerValue, probabilité qu'une mutation de caractéristique, d'un individu subisssant une mutation, survienne
            - fileName, nom de fichier pour les résultats.
            - origin, Valeur d'ilot pokemon.
            - population, Paramètre facultatif.
    """
    def compute(self, generationNb, genitorsPairsNb, pCross, pMut, pMutPerValue, fileName, origin, population = None):
        print("The evolutionary algorithm is running, please wait...")
        if (population == None):
            population = self.getPopulation()
        # Récupèration de la population initiale à partir d'un csv de pokemon
        for element in population:
            element.origin = origin
        generationIndex = 0
        # Copie simple.
        populationCopy = population[:]
        # Tant que le nombre de génération ne dépasse pas le nombre de génération max admis.
        while generationIndex < generationNb:
            # On récupère ici une liste de couples de géniteurs, tous choisis aléatoirement parmi la population.
            genitors = self.findGenitors(populationCopy, genitorsPairsNb)
            # On va évaluer chaque couple de géniteur afin de ne sélectionner que les meilleurs.
            bestGenitors = self.findBestGenitors(genitors)
            # Les meilleurs géniteurs subissent une variation (de type Mutation et/ou Croisement)
            # La liste retournée ci-dessous forme alors un ensemble "d'enfants"
            childrens = self.variation(copy.deepcopy(bestGenitors), pCross, pMut, pMutPerValue)
            # Des combats sont ensuite organisés entre les enfants et des membres de la population intiale
            populationCopy = self.replacement(populationCopy, childrens)
            # Ainsi, la population devient composée d'individus de plus en plus forts....
            # La "performance" de cette population finale dépend donc du nombre de générations (/nombre de remplacements effectués).
            generationIndex = generationIndex + 1
        print("Collection of the best Pokemon in progress...")
        # Triage auto des Pokemon selon leurs scores de combat : chaque pokemon affronte tous les autres.
        bestPokemons = self.findBestPokemon(populationCopy)
        # Exportation au format csv.
        self.exportBestPokemon(bestPokemons, fileName)
        return bestPokemons

    """
        Nom : getPopulation
        Description :
            Fonction permettant de récupèrer l'ensemble de la population initiale de pokemon.
            Cette fonction nous permet également de définir des variables qui serviront à créer nos propres combats.
        Renvoie : La liste des pokemons
    """
    def getPopulation(self):
        # Liste vide.
        pokemon = []
        # Lecture du csv pokemon
        pokemonDataCsv = pd.read_csv("data/basePokemon.csv", index_col=False)
        # Pour chaque pokemon existant...
        for i in range(len(pokemonDataCsv)) :
            # Transformation en objet pokemon.
            pokemon.append(TemplatePokemon(*extractFromLine(pokemonDataCsv.loc[i])))
        # Création de la liste des colonnes pour l'affichage de notre population (utilisé pour le csv).
        self.populationColumnsList = ['score', 'type1', 'type2', 'hp', 'attack', 'defense', 'specialAtk', 'specialDef', 'speed', 'legendary', 'id', 'origin']
        # On retourne la liste.
        return pokemon

    """
        Nom : findGenitors
        Description :
            Fonction permettant de trouver des couples de géniteurs aléatoires parmi une population.
        Paramètres :
            - population, Liste de pokemons représentant la population
            - genitorsPairsNb, Nombre de couples de géniteurs à trouver
        Renvoie : Une liste de couples de géniteurs
    """
    def findGenitors(self, population, genitorsPairsNb):
        genitorsPairs = []
        idList = []
        # Pour chaque géniteur à trouver...
        for i in range(genitorsPairsNb):
            # On recherche deux index aléatoires différents entre eux, borné par la taille de notre population.
            index1, index2 = self.findCoupleOfUniqueIndex(population, idList)
            # Création d'un nouveau couple de géniteur.
            genitorPair = (population[index1], population[index2])
            # Ajout de ce couple à la listes des couples de géniteurs.
            genitorsPairs.append(genitorPair)
        # On retourne un tableau contenant la liste des couples de géniteurs.
        return genitorsPairs

    """
        Nom : findBestGenitors
        Description :
            Fonction permettant de faire combattre (/évaluer) chaque couple de géniteur donné en paramètre.
            Et qui retourne la liste des meilleurs.
        Paramètres :
            - genitors, Liste des couples de géniteurs à évaluer
        Renvoie : Une liste des meilleurs géniteurs
    """
    def findBestGenitors(self, genitors):
        # Liste vide nous permettant de stocker les meilleurs géniteurs.
        bestGenitors = []
        # Liste de combat.
        genitorsPairs = []
        # Pour chaque couple de géniteur...
        for i in range(len(genitors)):
            # On récupère les deux pokemon sous la forme de stats brutes.
            genitorPair = np.concatenate((genitors[i][0].getPokemonAsArray(), genitors[i][1].getPokemonAsArray()))
            # Ajout des deux pokemon dans une liste de combat.
            genitorsPairs.append(genitorPair)
        # Création du dataFrame correspondant pour le réseau de neurones.
        genitorsDataFrame = pd.DataFrame(genitorsPairs)
        # Transformation d'une dataFrame en un ensemble de données pouvant être utilisé en prédiction pour notre réseau de neurones.
        genitorsList =  self.encodingUtility.createCustomSetForPrediction(genitorsDataFrame)
        # Récupèration des prédictions pour chaque combat.
        fightPrediction = pd.DataFrame(self.classifier.predict(genitorsList))
        # Pour chaque combat effectué...
        for i in range(len(fightPrediction)):
            # Si le geniteur 1 du couple i a plus de chances de gagner face au géniteur 2 alors...
            if(fightPrediction.iloc[i,0] > 0.5):
                # On récupère le geniteur 1 pour le placer dans la liste des meilleurs géniteurs.
                bestGenitors.append(genitors[i][0])
            else: # Sinon...
                # On récupère le geniteur 2 pour le placer dans la liste des meilleurs géniteurs.
                bestGenitors.append(genitors[i][1])
        # On retourne un tableau contenant la liste des meilleurs géniteurs.
        return bestGenitors

    """
        Nom : variation
        Description :
            Fonction permettant de faire varier un ensemble d'individus.
            L'ensemble des indivus résultant devient un ensemble d'enfants.
        Paramètres :
            - individuals, Liste des individus apte à subir une mutation et/ou un croisement.
            - pCross, probabilité qu'un individu soit croisé avec un autre individu parmi la liste des individus
            - pMut, probabilité qu'un individu subisse une mutation
            - pMutPerValue, probabilité qu'une mutation de caractéristique d'un individu survienne
        Renvoie : Une liste d'enfants
    """
    def variation(self, individuals, pCross, pMut, pMutPerValue):
        # Création d'une liste indvidualsWithMutations
        # Elle contiendra directement une liste, issue de notre liste d'individus initiale, avec un appel automatique à la fonction "mutation" (gestion des mutations) pour chaque ligne !
        indvidualsWithMutations = list(map(lambda p: self.mutation(p, pMut, pMutPerValue), individuals))
        # La liste va ensuite passer par une seconde fonction permettant de gérer les croisements.
        childrensList = self.crossover(indvidualsWithMutations, pCross)
        # On retourne finalement la liste des enfants obtenus (après mutation et croisement).
        return childrensList

    """
        Nom : mutation
        Description :
            Fonction permettant de vérifier si un individu va subir une mutation (selon une certaine probabilité).
            Si oui, la fonction retournera l'individu modifié.
        Paramètres :
            - individual, individu apte à subir une mutation.
            - pMut, probabilité qu'un individu subisse une mutation
            - pMutPerValue, probabilité qu'une mutation de caractéristique d'un individu survienne.
        Renvoie : Un individu, modifié ou non selon la probabilité de mutation
    """
    def mutation(self, individual, pMut, pMutPerValue):
        # Probablité qu'un individu subisse une mutation vérifiée ici.
        if random.random() < pMut :
            individual = individual.mutatePokemon(pMutPerValue)
        return individual

    """
        Nom : crossover
        Description :
            Fonction permettant de parcourir chaque indvidu d'une population avec un pas à 2.
            Pour chaque couple parcouru, on tente un croisement entre les deux individus du couple, selon une probabilité.
        Paramètres :
            - individuals, liste d'individus
            - pCross, probabilité qu'un individu soit croisé avec un autre individu parmi la liste des individus
        Renvoie : Un ensemble d'individus, modifié ou non selon la probabilité de croisement.
    """
    def crossover(self, individuals, pCross):
        # Pour chaque couple d'individu...
        for i in range(0, len(individuals)-1, 2):
            # Probablité que deux individus soient croisés vérifiée ici.
            if random.random() < pCross:
                # Appel à une fonction qui se charge de retourner un couple d'individu croisé.
                individuals[i], individuals[i+1] = individuals[i].crossover(individuals[i+1])
        # On retourne la liste des individus.
        return individuals

    """
        Nom : replacement
        Description :
            Fonction permettant d'effectuer un remplacement entre une population de base et des enfants.
            Ce remplacement se base sur une évaluation par combat de pokemon.

            Chaque enfant va affronter un membre de la population aléatoire (sans que deux enfants n'affronte le même).
            A l'issue de chaque combat, le gagnant possède une place dans la nouvelle population, retournée en fin de fonction

            Ainsi, si un enfant gagne face à un membre de la population existant, il prend sa place.
            A l'inverse, si le membre de la population gagne, il garde sa place et l'enfant est oublié.
        Paramètres :
            - population, ensemble de la population
            - childrens, ensemble d'enfants
        Renvoie : la nouvelle population, fruit des meilleurs en combat : population existante vs enfants.
    """
    def replacement(self, population, childrens):
        # Création d'une liste d'index de population qui permettra de connaitre quel individu a déja été pris en combat ou non.
        populationIndexList = []
        # Liste qui stockera les combats à prédire.
        fightList = []
        # Liste qui stockera les index de pokemon en combat.
        indexList = []
        # Pour chaque enfant existant...
        for i in range(len(childrens)):
            # On recherche un nouvel adversaire parmi la population.

            # Le code ci-dessous permet de choisir le remplacement par espèce.
            """
            for j in range(len(population)):
                if population[j].id == childrens[i].id:
                    indexPopulation = j
            """
            # Le code ci-dessous permet de choisir le remplacement aléatoire.
            indexPopulation = self.findUniqueIndex(population, populationIndexList)

            # On créé un ensemble de données de combat (regroupant l'enfant et le pokemon faisant partie de la population actuelle).
            newPopulationFight = np.concatenate((childrens[i].getPokemonAsArray(), population[indexPopulation].getPokemonAsArray()))
            indexList.append((i, indexPopulation))
            # Ajout de cette ligne dans le tableau des combats.
            fightList.append(newPopulationFight)
        # La nouvelle population possédera déjà tous les individus non concernés par les combats.
        newPopulation = population[:]
        # Création d'un dataFrame correspondant au combats à prédire.
        fightDataFrame = pd.DataFrame(fightList)
        # Préparation des données avant prédiction par le réseau de neurones.
        fightList = self.encodingUtility.createCustomSetForPrediction(fightDataFrame)
        # Récupèration des prédictions.
        fightPrediction = pd.DataFrame(self.classifier.predict(fightList))
        # Création d'une table de présence par espèce de Pokémon pour notre fitness sharing.
        newPopulationValues = pd.DataFrame(newPopulation)["id"].value_counts()
        # Pour chaque prédiction de combat (et donc pour chaque couple de pokemon concerné)...
        for i in range(len(fightPrediction)):
            # On fait le rapport entre l'espèce sur la population totale.

            # La ligne ci-dessous permet de définir le remplacement par fitness Sharing.
            fitnessSharingFactor = newPopulationValues.loc[childrens[indexList[i][0]].id] / len(newPopulation)

            # Si le pokemon 1 du couple a plus de chances de gagner face au pokemon 2 alors...
            if(fightPrediction.iloc[i,0] - fitnessSharingFactor > 0.5):
                # On modifie la table de présence pour le Pokémon battu. (utilisée pour la fitness Sharing)
                newPopulationValues.loc[newPopulation[indexList[i][1]].id] = newPopulationValues.loc[newPopulation[indexList[i][1]].id]-1
                # le pokemon gagnant gagne sa place.
                newPopulation[indexList[i][1]] = childrens[indexList[i][0]]
                # On modifie la table de présence pour le Pokémon gagnant. (utilisée pour la fitness Sharing)
                newPopulationValues.loc[childrens[indexList[i][0]].id] = newPopulationValues.loc[childrens[indexList[i][0]].id]+1
        # On retourne un tableau contenant la liste des gagnants : c'est la nouvelle génération.
        return newPopulation

    """
        Nom : findBestPokemon
        Description :
            Fonction permettant de trier une population selon leurs capacités en combat.
        Paramètres :
            - population, liste de pokemons
        Renvoie : une liste triée : le premier pokemon est celui ayant gagné le plus de combats.
    """
    def findBestPokemon(self, population):
        # Liste qui stockera les combats à prédire.
        fightList = []
        # Liste qui stockera tous les index de combat.
        indexList = []
        for i in range(len(population)):
            for j in range(i):
                # Sauvegarde des index de combat.
                indexList.append((i,j))
                # Création d'un combat à prédire.
                newPopulationFight = np.concatenate((population[i].getPokemonAsArray(), population[j].getPokemonAsArray()))
                # Ajout du combat dans un tableau.
                fightList.append(newPopulationFight)
        # Création d'un dataFrame correspondant aux combats à prédire.
        fightDataFrame = pd.DataFrame(fightList)
        # Préparation des données avant prédiction par le réseau de neurones.
        fightList = self.encodingUtility.createCustomSetForPrediction(fightDataFrame)
        # Récupèration des prédictions.
        fightPrediction = pd.DataFrame(self.classifier.predict(fightList))
        # On remet les scores à 0 entre générations.
        for p in population:
            p.score = 0
        # Pour chaque prédiction de combat (et donc pour chaque couple de pokemon concerné)...
        for i in range(len(fightPrediction)):
            # Si le pokemon 1 du couple a plus de chances de gagner face au pokemon 2 alors...
            if(fightPrediction.iloc[i,0] > 0.5):
                # On incremente le score du pokemon concerné.
                population[indexList[i][0]].score = population[indexList[i][0]].score + 1
            else: # Sinon...
                # On incremente le score de l'autre Pokemon.
                population[indexList[i][1]].score = population[indexList[i][1]].score + 1
        # Triage des Pokemon selon leur score : Ordre décroissant.
        population.sort(key=lambda x: x.score, reverse=True)
        return population

    """
        Nom : findCoupleOfUniqueIndex
        Description :
            Fonction permettant de récupèrer un couple d'index aléatoires différents entre eux.
            L'index aléatoire doit se baser sur une liste.
            Cette fonction intègre également une liste d'index en paramètre, permettant de nous assurer que les géniteurs sont toujours différents d'un couple à l'autre.
        Paramètres :
            - list, liste de données quelconque à utiliser pour trouver deux index.
            - idList, liste d'index, utilisé pour isoler les index jamais utilisés auparavant.
        Renvoie : un couple d'indices aléatoires différents entre eux.
    """
    def findCoupleOfUniqueIndex(self, list, idList):
        id1 = random.randint(0,len(list)-1)
        id2 = random.randint(0,len(list)-1)
        while id1 in idList or id2 in idList or id1 == id2:
            id1 = random.randint(0,len(list)-1)
            id2 = random.randint(0,len(list)-1)
        idList.append(id1)
        idList.append(id2)
        return id1, id2

    """
        Nom : findUniqueIndex
        Description :
            Fonction permettant de retourner un index quelconque d'une liste, sans que celui-ci soit présent dans une seconde liste donnée en paramètre.
        Paramètres :
            - list, liste de données quelconque à utiliser pour trouver un index.
            - indexList, la liste des index à ne pas retourner.
        Renvoie : un nouveau index, non présent dans indexList.
    """
    def findUniqueIndex(self, list, indexList):
        index = random.randint(0,len(list)-1)
        while index in indexList:
            index = random.randint(0,len(list)-1)
        indexList.append(index)
        return index

    """
        Nom : exportBestPokemon
        Description :
            Fonction permettant d'exporter les meilleurs Pokemon au format csv.
            Cette même fonction va également calculer la valeur de stat total pour chaque pokemon concerné.
        Paramètres :
            - population : population à exporter.
            - fileName, nom de fichier pour l'exportation.
    """
    def exportBestPokemon(self, population, fileName):
        # Récupèration des colonnes sous la forme d'un dataframe avec valeurs d'affichages (stats, id, etc)
        sortedDataframe = pd.DataFrame(list(map(lambda p: p.getPokemonForView(), population)), columns=self.populationColumnsList)
        # Conversion en liste pour le calcul de stat total.
        columnList = list(sortedDataframe)
        # On enlève les colonnes non utilisées pour ce calcul.
        columnList.remove("type1")
        columnList.remove("type2")
        columnList.remove("id")
        columnList.remove("score")
        columnList.remove("legendary")
        # Ajout d'une nouvelle colonne dans le dataFrame.
        sortedDataframe.insert(3, "total", 0.0)
        # Calcul des somme de stat pour chaque ligne avec ajout des valeurs dans le dataFrame.
        sortedDataframe["total"] = sortedDataframe[columnList].sum(axis=1)
        # Exportation au format csv.
        sortedDataframe.to_csv(fileName+'.csv', index=False)