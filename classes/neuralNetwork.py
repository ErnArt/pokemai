from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from sklearn.metrics import confusion_matrix

class NeuralNetwork:
    """
        Nom : Constructeur
        Description :
            Initialisation du réseau de neurones.
        Paramètres : 
            -data, les données brutes en entrée pour adapter ci-besoin les paramètres du réseau de neurones.
    """
    def __init__(self, data):
        self.data = data
        # Creation du réseau (vide).
        self.classifier = Sequential()
        # Ajout Couche en entrée de réseau.
        self.classifier.add(Dense(units = len(self.data.columns), kernel_initializer = 'uniform', activation = 'relu', input_dim = (len(self.data.columns)-1)))
        self.classifier.add(Dropout(0.1))

        # Ajout Couche cachée.
        self.classifier.add(Dense(units = len(self.data.columns), kernel_initializer = 'uniform', activation = 'relu'))
        self.classifier.add(Dropout(0.1))

        # Ajout Couche cachée.
        self.classifier.add(Dense(units = len(self.data.columns), kernel_initializer = 'uniform', activation = 'relu'))
        self.classifier.add(Dropout(0.1))

        # Ajout Couche en sortie de réseau.
        self.classifier.add(Dense(units = 1, kernel_initializer = 'uniform', activation = 'sigmoid'))

        # Compilation du réseau.
        self.classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

    """
        Nom : train
        Description :
            Permet d'entrainer le réseau de neurones.
        Paramètres : 
            -xTrain, le set de données en entrée pour le training
            -yTrain, le set de données en sortie pour le training
            -batch_size, la taille de chaque batch (à mieux définir !) pour l'entrainement de notre réseau de neurones 
            -epochs, le nombres de d'epochs pour l'entrainement notre réseau de neurones 
    """
    def train(self, xTrain, yTrain, batch_size, epochs):
        self.classifier.fit(xTrain, yTrain, batch_size = batch_size, epochs = epochs)

    """
        Nom : predict
        Description :
            Permet de prédire un set de données yPred selon un set de données en entrée.
            Prédiction de sortie -> Probabilité de succés
        Paramètres : 
            -xTrain, le set de données en entrée pour la prédiction
        Renvoie : yPred, le set de données comprenant les probabilités de succés.
    """
    def predict(self, xTest):
        yPred = self.classifier.predict(xTest)
        yPred = (yPred > 0.5)
        return yPred

    """
        Nom : createConfusionMatric
        Description :
            Permet de renvoyer la matrice de confusion, indicateur de précision pour notre réseau de neurones.
        Paramètres : 
            -xTrain, le set de données en entrée pour la prédiction
        Renvoie : la matrice de confusion correspondante.
    """
    def createConfusionMatric(self, yTest, yPred):
        return confusion_matrix(yTest, yPred)