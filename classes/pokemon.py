from dataclasses import dataclass
from typing import List
import math
import random
import copy

"""
	Nom : extractFromLine
	Description : 
		Fonction récupérant de line les différentes caractéristiques du Pokémon. 
		Si la nature n'est pas inclue, elle renvoie None pour cette dernière ainsi que pour le niveau.
		Ce cas est possible uniquement quand on créé un Pokémon à partir des modèles de base.
	Paramètres :
		- line, ligne du dataframe.
"""
def extractFromLine(line):
	try:
		line[11]
		return line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8], line[9], line[10], line[11], line[12]
	except IndexError:
		return line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8], line[9], None, None, line[10]

"""
	Nom : intToNature
	Description : 
		Fonction renvoyant pour une nature en entier son nom.
	Paramètres :
		- intValue, int représentant la nature.
"""
def intToNature(intValue):
	cases = {
		1:	"Bold",
		2:	"Quirky",
		3:	"Brave",
		4:	"Calm",
		5:	"Quiet",
		6:	"Docile",
		7:	"Mild",
		8:	"Rash",
		9:	"Gentle",
		10:	"Hardy",
		11:	"Jolly",
		12:	"Lax",
		13:	"Impish",
		14:	"Sassy",
		15:	"Naughty",
		16:	"Modest",
		17:	"Naive",
		18:	"Hasty",
		19:	"Careful",
		20:	"Bashful",
		21:	"Relaxed",
		22:	"Adamant",
		23:	"Serious",
		24:	"Lonely",
		25:	"Timid",
	}
	return cases.get(intValue)

"""
	Nom : natureToInt
	Description : 
		Fonction renvoyant pour une nature son entier.
	Paramètres :
		- nature, string représentant la nature.
"""
def natureToInt(nature):
	cases = {
		"Bold": 	1,
		"Quirky": 	2,
		"Brave": 	3,
		"Calm": 	4,
		"Quiet": 	5,
		"Docile": 	6,
		"Mild": 	7,
		"Rash": 	8,
		"Gentle": 	9,
		"Hardy": 	10,
		"Jolly": 	11,
		"Lax": 		12,
		"Impish": 	13,
		"Sassy": 	14,
		"Naughty": 	15,
		"Modest": 	16,
		"Naive": 	17,
		"Hasty": 	18,
		"Careful": 	19,
		"Bashful": 	20,
		"Relaxed": 	21,
		"Adamant": 	22,
		"Serious": 	23,
		"Lonely": 	24,
		"Timid": 	25,
	}
	return cases.get(nature)


"""
	Nom : pickRandomNature
	Description : 
		Fonction renvoyant une nature aléatoire.
"""
def pickRandomNature():
	return intToNature(random.randint(1, 25))

"""
	Nom : natureToMultiplier
	Description : 
		Fonction renvoyant pour une nature et sa statistique le multiplicateur utilisé pour le calcul de statistiques.
		On utilise un équivalent de double while pour aisément renvoyer les bons multiplicateurs.
	Paramètres :
		- nature, int représentant la nature.
		- stat, string représentant la statistique pour laquelle on recherche le multiplicateur.
"""
def natureToMultiplier(nature, stat):
	cases = {
		1:	{
			"attack":		0.9,
			"defense":		1.1,
			"specialAtk":	1,
			"specialDef":	1,
			"speed":		1,
		},
		2:	{
			"attack":		1,
			"defense":		1,
			"specialAtk":	1,
			"specialDef":	1,
			"speed":		1,
		},
		3:	{
			"attack":		1.1,
			"defense":		1,
			"specialAtk":	1,
			"specialDef":	1,
			"speed":		0.9,
		},
		4:	{
			"attack":		0.9,
			"defense":		1,
			"specialAtk":	1,
			"specialDef":	1.1,
			"speed":		1,
		},
		5:	{
			"attack":		1,
			"defense":		1,
			"specialAtk":	1.1,
			"specialDef":	1,
			"speed":		0.9,
		},
		6:	{
			"attack":		1,
			"defense":		1,
			"specialAtk":	1,
			"specialDef":	1,
			"speed":		1,
		},
		7:	{
			"attack":		1,
			"defense":		0.9,
			"specialAtk":	1.1,
			"specialDef":	1,
			"speed":		1,
		},
		8:	{
			"attack":		1,
			"defense":		1,
			"specialAtk":	1.1,
			"specialDef":	0.9,
			"speed":		1,
		},
		9:	{
			"attack":		1,
			"defense":		0.9,
			"specialAtk":	1,
			"specialDef":	1.1,
			"speed":		1,
		},
		10:	{
			"attack":		1,
			"defense":		1,
			"specialAtk":	1,
			"specialDef":	1,
			"speed":		1,
		},
		11:	{
			"attack":		1,
			"defense":		1,
			"specialAtk":	0.9,
			"specialDef":	1,
			"speed":		1.1,
		},
		12:	{
			"attack":		1,
			"defense":		1.1,
			"specialAtk":	1,
			"specialDef":	0.9,
			"speed":		1,
		},
		13:	{
			"attack":		1,
			"defense":		1.1,
			"specialAtk":	0.9,
			"specialDef":	1,
			"speed":		1,
		},
		14:	{
			"attack":		1,
			"defense":		1,
			"specialAtk":	1,
			"specialDef":	1.1,
			"speed":		0.9,
		},
		15:	{
			"attack":		1.1,
			"defense":		1,
			"specialAtk":	1,
			"specialDef":	0.9,
			"speed":		1,
		},
		16:	{
			"attack":		0.9,
			"defense":		1,
			"specialAtk":	1.1,
			"specialDef":	1,
			"speed":		1,
		},
		17:	{
			"attack":		1,
			"defense":		1,
			"specialAtk":	1,
			"specialDef":	0.9,
			"speed":		1.1,
		},
		18:	{
			"attack":		1,
			"defense":		0.9,
			"specialAtk":	1,
			"specialDef":	1,
			"speed":		1.1,
		},
		19:	{
			"attack":		1,
			"defense":		1,
			"specialAtk":	0.9,
			"specialDef":	1.1,
			"speed":		1,
		},
		20:	{
			"attack":		1,
			"defense":		1,
			"specialAtk":	1,
			"specialDef":	1,
			"speed":		1,
		},
		21:	{
			"attack":		1,
			"defense":		1.1,
			"specialAtk":	1,
			"specialDef":	1,
			"speed":		0.9,
		},
		22:	{
			"attack":		1.1,
			"defense":		1,
			"specialAtk":	0.9,
			"specialDef":	1,
			"speed":		1,
		},
		23:	{
			"attack":		1,
			"defense":		1,
			"specialAtk":	1,
			"specialDef":	1,
			"speed":		1,
		},
		24:	{
			"attack":		1.1,
			"defense":		0.9,
			"specialAtk":	1,
			"specialDef":	1,
			"speed":		1,
		},
		25:	{
			"attack":		0.9,
			"defense":		1,
			"specialAtk":	1,
			"specialDef":	1,
			"speed":		1.1,
		},
	}
	return cases.get(nature).get(stat)


@dataclass
class Pokemon:
	id:				int
	name:			str
	type1:			str
	type2:			str
	hp:				int
	attack:			int
	defense:		int
	specialAtk:		int
	specialDef:		int
	speed:			int
	nature:			str
	level:			int=100
	score:			int=0
	"""
		Nom : Constructeur
		Paramètres :
			- id, int représentant l'id du Pokémon.
			- name, string représentant le nom du Pokémon.
			- attack, int représentant l'attaque du Pokémon.
			- defense, int représentant la défense du Pokémon.
			- specialAtk, int représentant l'attaque spéciale du Pokémon.
			- specialDefense, int représentant la défense spéciale du Pokémon.
			- speed, int représentant la vitesse du Pokémon.
			- nature, string représentant la nature du Pokémon.
			- level, int représentant le niveau du Pokémon.
			- legendary, booléen précisant si le pokemon est légendaire ou non.
	"""
	def __init__(self, id, name, type1, type2, hp, attack, defense, 
		specialAtk, specialDef, speed, nature, level, legendary):
		self.id = id
		self.name = name
		self.type1 = type1
		self.type2 = type2
		self.hp = hp
		self.attack = attack
		self.defense = defense
		self.specialAtk = specialAtk
		self.specialDef = specialDef
		self.speed = speed
		self.nature = nature
		self.level = level
		self.legendary = legendary
		self.score = 0

@dataclass
class TemplatePokemon(Pokemon):
	ivArray = []
	evArray = []

	"""
		Nom : Constructeur
		Paramètres :
			- id, int représentant l'id du Pokémon.
			- name, string représentant le nom du Pokémon.
			- attack, int représentant l'attaque du Pokémon.
			- defense, int représentant la défense du Pokémon.
			- specialAtk, int représentant l'attaque spéciale du Pokémon.
			- specialDefense, int représentant la défense spéciale du Pokémon.
			- speed, int représentant la vitesse du Pokémon.
			- nature, string représentant la nature du Pokémon.
			- level, int représentant le niveau du Pokémon.
			- legendary, booléen précisant si le pokemon est légendaire ou non.
	"""
	def __init__(self, id, name, type1, type2, hp, attack, defense, 
		specialAtk, specialDef, speed, nature, level, legendary):
		super(TemplatePokemon, self).__init__(id, name, type1, type2, hp, attack, defense, 
		specialAtk, specialDef, speed, nature, level, legendary)
		self.nature = pickRandomNature()
		self.level = 100
		self.ivArray = [0, 0, 0, 0, 0, 0]
		self.evArray = [0, 0, 0, 0, 0, 0]
		self.generateRandomIV()
		self.generateRandomEV()
		self.origin = 0

	def __str__(self):
		baseString = "TemplatePokemon(id={}, name='{}', type1='{}', type2='{}', hp={}, attack={}, defense={}, specialAtk={}, specialDef={}, speed={}, nature='{}', level={}"
		baseString = baseString.format(self.id, self.name, self.type1, self.type2, self.hp, self.attack, self.defense, self.specialAtk, self.specialDef, self.speed, self.nature, self.level)
		baseString += ", ivHp={}, ivAttack={}, ivDefense={}, ivSpecialAtk={}, ivSpecialDef={}, ivSpeed={}".format(self.ivArray[0], self.ivArray[1], self.ivArray[2], self.ivArray[3], self.ivArray[4], self.ivArray[5])
		baseString += ", evHp={}, evAttack={}, evDefense={}, evSpecialAtk={}, evSpecialDef={}, evSpeed={})".format(self.evArray[0], self.evArray[1], self.evArray[2], self.evArray[3], self.evArray[4], self.evArray[5])
		return baseString

	"""
		Nom : generateRandomIV
		Description :
			Fonction qui génère des IV aléatoires entre 0 et 31 pour chaque statistiques.
	"""
	def generateRandomIV(self):
		for i in range(0, 6):
			self.ivArray[i] = random.randint(0, 31)

	"""
		Nom : generateRandomEV
		Description :
			Fonction qui génère des EV aléatoires entre 0 et 252 pour chaque statistiques.
			Elle respecte le total de 510.
	"""
	def generateRandomEV(self):
		tabTmp = [0, 0, 0, 0, 0, 0]
		max = 510
		for i in range(len(tabTmp)):
			tabTmp[i] = random.randint(0, 100)
		for i in range(len(self.evArray)):
			self.evArray[i] = int(tabTmp[i] * max / sum(tabTmp))

	"""
		Nom : mutatePokemon
		Description :
			Fonction permettant de faire muter un pokemon directement, tout en retournant une nouvelle instance du pokemon muté.
		Paramètre :
			pMutPerValue, probabilité qu'une caractéristique soit mutée.
		Renvoie :
			le nouveau pokemon muté.
	"""
	def mutatePokemon(self, pMutPerValue):
		# Création d'une copie des ev.
		evArrayC = copy.copy(self.evArray)
		# Pour chaque valeur d'EV...
		for i in range (len(evArrayC)):
			# Vérification de probabilité.
			if(random.random() < pMutPerValue):
				# Si la caractéristique est positive
				if evArrayC[i] > 0 :
					# Variation positive ou négative
					if(random.random() < 0.75):
						evArrayC[i] += random.randint(1, 5)
					else:
						evArrayC[i] -= random.randint(1, 5)
				# Si la caractéristique est négative...
				while evArrayC[i] <= 0 :
					evArrayC[i] += random.randint(1, 5)
				# Si la caractéristique dépasse la limite fixée.
				while evArrayC[i] > 252:
					evArrayC[i] -= random.randint(1, 5)
		# Si la somme des ev dépasse 510, on tente une correction...
		while(sum(evArrayC) > 510):
			# Caractéristique aléatoire.
			randInt = random.randint(0, 5)
			# Si cette caractéristique n'est pas trop basse...
			if(evArrayC[randInt] > 5):
				# Elle perd en valeur afin d'atteindre 510 en total.
				evArrayC[randInt] -= random.randint(1, 5)

		# Création d'une copie de pokemon.
		pokemonC = copy.deepcopy(self)
		# Le nouveau pokemon récupère les ev calculés.
		pokemonC.evArray = evArrayC
		# On retourne le nouveau pokemon.
		return pokemonC

	"""
		Nom : crossover
		Description :
			Fonction permettant de faire un croisement entre deux pokemon.
		Paramètre :
			pokemon2, pokemon n°2 dans le process de croisement.
		Renvoie :
			les deux nouveaux pokemon.
	"""
	def crossover(self, pokemon2):
		# Création d'une copie des iv du pokemon 1.
		ivCopy = copy.copy(self.ivArray)
		# Création d'une copie des iv du pokemon 2.
		pokemon2IvCopy = copy.copy(pokemon2.ivArray)
		# Choix d'un index entre 1 et 4, pivot de croisement.
		ivIndex = random.randint(1, 4)
		# Probabilité que le croisement se fasse dans un sens ou dans l'autre.
		if(random.random() < 0.5):
			# Sens 1 -> Croisement à partir du pivot jusqu'à la fin de tableau.
			for j in range(ivIndex, len(ivCopy)):
				ivCopy[j], pokemon2IvCopy[j] = pokemon2IvCopy[j], ivCopy[j]
		else:
			# Sens 2 -> Croisement à partir du début de tableau jusqu'au pivot.
			for j in range(0, ivIndex):
				ivCopy[j], pokemon2IvCopy[j] = pokemon2IvCopy[j], ivCopy[j]

		# On passe au croisement des ev.
		# Création d'une copie des ev du pokemon 1.
		evCopy = copy.copy(self.evArray)
		# Création d'une copie des ev du pokemon 2.
		pokemon2EvCopy = copy.copy(pokemon2.evArray)
		# Choix d'un index entre 1 et 4, pivot de croisement.
		evIndex = random.randint(1, 4)
		# Probabilité que le croisement se fasse dans un sens ou dans l'autre.
		if(random.random() < 0.5):
			for j in range(evIndex, len(evCopy)):
				evCopy[j], pokemon2EvCopy[j] = pokemon2EvCopy[j], evCopy[j]
		else:
			for j in range(0, evIndex):
				evCopy[j], pokemon2EvCopy[j] = pokemon2EvCopy[j], evCopy[j]

		# Index d'occurences : permet de connaître le nombre de tests effectués afin de trouver un croisement d'ev valide.
		# Pour le moment, un seul test a été effectué...
		i = 1
		# Tant que l'une des valeurs totale d'ev est supérieure à 510, on considère le croisement comme invalide...
		# Le nombre de tests maximum pour réaliser un croisement d'ev est défini par : i < max.
		while((sum(evCopy) > 510 or sum(pokemon2EvCopy) > 510) and i < 100):
			# On recommence un nouveau process de croisement d'ev.
			# Création d'une copie des ev du pokemon 1.
			evCopy = copy.copy(self.evArray)
			# Création d'une copie des ev du pokemon 2.
			pokemon2EvCopy = copy.copy(pokemon2.evArray)
			# Choix d'un index entre 1 et 5, indicateur de croisement.
			evIndex = random.randint(1, 4)
			# Probabilité que le croisement se fasse dans un sens ou dans l'autre.
			if(random.random() < 0.5):
				for j in range(evIndex, len(evCopy)):
					evCopy[j], pokemon2EvCopy[j] = pokemon2EvCopy[j], evCopy[j]
			else:
				for j in range(0, evIndex):
					evCopy[j], pokemon2EvCopy[j] = pokemon2EvCopy[j], evCopy[j]
			# Un nouveau test de croisement passé...
			i += 1

		# On réalise une copie du pokemon 1.
		selfC = copy.deepcopy(self)
		# On réalise une copie du pokemon 2.
		pokemon2C = copy.deepcopy(pokemon2)
		# Le nouveau pokemon 1 récupère les iv croisés lui correspondant.
		selfC.ivArray = ivCopy
		# Le nouveau pokemon 2 récupère les iv croisés lui correspondant.
		pokemon2C.ivArray = pokemon2IvCopy
		# Si les tableaux d'ev respectent le total à 510, le croisement est valide (réalisé à temps).
		# Dans le cas contraire, on considère qu'un croisement des ev est impossible, à moins de fournir plus de tests.
		if (sum(evCopy) <= 510 and sum(pokemon2EvCopy) <= 510):
			# Le nouveau pokemon 1 récupère les ev croisés lui correspondant.
			selfC.evArray = evCopy
			# Le nouveau pokemon 2 récupère les ev croisés lui correspondant.
			pokemon2C.evArray = pokemon2EvCopy
		# On retourne les nouveaux pokemon.
		return selfC, pokemon2C

	"""
		Nom : processSpecificStat
		Description : 
			Fonction renvoyant pour une stat le calcule de la statistique avec IV, EV, nature et niveau.
		Paramètres :
			- stat, string représentant la statistique désirée.
			- ivStat, int représentant l'iv de la statistique désirée.
			- evStat, int représentant l'ev de la statistique désirée.
			- baseStat, int représentant la valeur de base de la statistique désirée.
	"""
	def processSpecificStat(self, stat, ivStat, evStat, baseStat):
		if (stat == "hp"):
			return ((ivStat + 2*baseStat + math.floor(evStat/4)) * math.floor(self.level/100)) + self.level + 10
		return (((ivStat + 2*baseStat + math.floor(evStat/4)) * math.floor(self.level/100)) + 5) * natureToMultiplier(natureToInt(self.nature), stat)

	"""
		Nom : processStat
		Description : 
			Fonction renvoyant pour une stat le calcule de la statistique avec IV, EV, nature et niveau.
			Cette fonction utilise la fonction précédente pour être utilisée de façon simplifiée par le développeur.
		Paramètres :
			- stat, string représentant la statistique désirée.
	"""
	def processStat(self, stat):
		if 		(stat == "hp"):
			return math.floor(self.processSpecificStat(stat, self.ivArray[0], self.evArray[0], self.hp))
		elif	(stat == "attack"):
			return math.floor(self.processSpecificStat(stat, self.ivArray[1], self.evArray[1], self.attack))
		elif	(stat == "defense"):
			return math.floor(self.processSpecificStat(stat, self.ivArray[2], self.evArray[2], self.defense))
		elif	(stat == "specialAtk"):
			return math.floor(self.processSpecificStat(stat, self.ivArray[3], self.evArray[3], self.specialAtk))
		elif	(stat == "specialDef"):
			return math.floor(self.processSpecificStat(stat, self.ivArray[4], self.evArray[4], self.specialDef))
		elif	(stat == "speed"):
			return math.floor(self.processSpecificStat(stat, self.ivArray[5], self.evArray[5], self.speed))

	"""
		Nom : getProcessedPokemon
		Description :
			Fonction calculant les statistiques du Pokémon avec ses IV, EV, nature et niveau.
		Renvoie : Un Pokémon de la classe mère avec les statistiques calculées pour être utilisé en test.
	"""
	def getProcessedPokemon(self):
		processedHp 		= self.processStat("hp")
		processedAttack 	= self.processStat("attack")
		processedDefense 	= self.processStat("defense")
		processedSpecialAtk = self.processStat("specialAtk")
		processedSpecialDef = self.processStat("specialDef")
		processedSpeed 		= self.processStat("speed")
		return Pokemon(self.id, self.name, self.type1, self.type2, processedHp, processedAttack, processedDefense, 
			processedSpecialAtk, processedSpecialDef, processedSpeed, self.nature, self.level, self.legendary)

	"""
		Nom : getPokemonAsArray
		Description :
			Fonction calculant les statistiques du Pokémon avec ses IV, EV, nature et niveau.
		Renvoie : Un Pokémon de la classe mère avec les statistiques calculées pour être utilisé dans le réseau de neurones.
	"""
	def getPokemonAsArray(self):
		processedHp 		= self.processStat("hp")
		processedAttack 	= self.processStat("attack")
		processedDefense 	= self.processStat("defense")
		processedSpecialAtk = self.processStat("specialAtk")
		processedSpecialDef = self.processStat("specialDef")
		processedSpeed 		= self.processStat("speed")
		return [self.type1, self.type2, processedHp, processedAttack, processedDefense, processedSpecialAtk, processedSpecialDef, processedSpeed, self.legendary]

	"""
		Nom : getPokemonForView
		Description :
			Fonction calculant les statistiques du Pokémon avec ses IV, EV, nature et niveau.
		Renvoie : Un Pokémon de la classe mère avec les statistiques calculées pour être utilisé en affichage.
	"""
	def getPokemonForView(self):
		processedHp 		= self.processStat("hp")
		processedAttack 	= self.processStat("attack")
		processedDefense 	= self.processStat("defense")
		processedSpecialAtk = self.processStat("specialAtk")
		processedSpecialDef = self.processStat("specialDef")
		processedSpeed 		= self.processStat("speed")
		return [self.score, self.type1, self.type2, processedHp, processedAttack, processedDefense, processedSpecialAtk, processedSpecialDef, processedSpeed, self.legendary, self.id, self.origin]