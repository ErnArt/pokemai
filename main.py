from sklearn.preprocessing import StandardScaler
from classes import dataUtility
from classes import encodingUtility 
from classes import neuralNetwork 
from classes import evolutionaryAlgorithm 
from classes import evolutionaryAlgorithm_v2
from natsort import natsorted

import time
import os, pathlib
import pandas as pd
import json

"""
    Projet : PokemAI
    Master 1 WEB
    Développeurs :  - Ernesto Artigas
                    - Alexandre Fovet
                    - Arthur Ringot
"""

### Objets Python
# Standardisation de notre set de données.
standardScaler = StandardScaler()
# Utilitaire d'encodage.
encodingUtility = encodingUtility.EncodingUtility(standardScaler)
# Utilitaire de traitement de données.
dataUtility = dataUtility.DataUtility("data/fights.csv", encodingUtility, standardScaler)

### MAIN
# Importation des données brutes.
data = dataUtility.importData()

## Création du réseau de neurones
neuralNetwork = neuralNetwork.NeuralNetwork(data)

## Initialisation de l'algorithme évolutionnaire v1
evolutionaryAlgorithmv1 = evolutionaryAlgorithm.EvolutionaryAlgorithm(neuralNetwork, encodingUtility)
evolutionaryAlgorithmv2 = evolutionaryAlgorithm_v2.EvolutionaryAlgorithmV2(neuralNetwork, encodingUtility)

### Méthodes
def computeEvolutionaryAlgorithmSimulationV1(data, nSims, generationNb, genitorsPairsNb, pCross, pMut, pMutPerValue):
    ## Lancement de l'algorithme évolutionnaire v1 nSims fois.
    for i in range(nSims):
        evolutionaryAlgorithmv1.compute(data, generationNb, genitorsPairsNb, pCross, pMut, pMutPerValue, ('results/v1/result_' + str(i)))

### Méthodes
def computeEvolutionaryAlgorithmSimulationV2(nSims, generationNb, genitorsPairsNb, pCross, pMut, pMutPerValue):
    ## Lancement de l'algorithme évolutionnaire v2 nSims fois.
    for i in range(nSims):
        evolutionaryAlgorithmv2.compute(generationNb, genitorsPairsNb, pCross, pMut, pMutPerValue, ('results/v2/basic/result_' + str(i)), i)

def computeEvolutionaryAlgorithmSimulationCoEvolution(nSims, generationNb, genitorsPairsNb, pCross, pMut, pMutPerValue):
    ## Lancement de l'algorithme évolutionnaire nSims fois.
    population = evolutionaryAlgorithmv2.getPopulation()
    bestPokemonResults = []
    print("Processing populations.")
    for i in range(nSims):
        # Algorithme évolutionnaire avec exportation des résultats.
        bestPokemonResults.append(evolutionaryAlgorithmv2.compute(generationNb, genitorsPairsNb, pCross, pMut, pMutPerValue, ('results/v2/coEvolution/beforeSwap/result_' + str(i)), i, population))
        print("Island", str(i), "complete.")

    # Swap sur la moitié du tableau pair.
    print("Processing swap.")
    for i in range (0, int(len(bestPokemonResults)/2)):
        bestPokemonResults[i][0], bestPokemonResults[(len(bestPokemonResults)-1)-i][0] =  bestPokemonResults[(len(bestPokemonResults)-1)-i][0], bestPokemonResults[i][0]

    # Organisation de nouveaux combats pour exportation après swap.
    print("Processing new battles.")
    for i in range (0, len(bestPokemonResults)):
        tempPopulation = evolutionaryAlgorithmv2.findBestPokemon(bestPokemonResults[i])
        evolutionaryAlgorithmv2.exportBestPokemon(tempPopulation, 'results/v2/coEvolution/afterSwap/result_' + str(i))

    results1 = processingResultsWinnerSwapped()
    results2 = processingNumberWinner()
    results3 = processingNumberWinnerAfterSwap()

    print("Saving results as json.")
    with open("results/v2/coEvolution/json/resultsWinnerSwapped.json", "w") as file:
        json.dump(results1, file)
    with open("results/v2/coEvolution/json/resultsNumberWinner.json", "w") as file:
        json.dump(results2, file)
    with open("results/v2/coEvolution/json/resultsNumberWinnerAfterSwap.json", "w") as file:
        json.dump(results3, file)

def fetchingResultsToArray(folderName):
    filesArray = []
    for filename in natsorted(os.listdir(folderName)):
        filesArray.append(pd.read_csv(folderName+"/"+filename))
    return filesArray

# conversion to int instead of int64 because can't jsonify it.
def auxProcessingNumberWinner(filesArray):
    numberWinner = {}
    for element in filesArray:
        try:
            numberWinner[int(element.id[0])]
        except:
            numberWinner[int(element.id[0])] = 0
        numberWinner[int(element.id[0])] = numberWinner[int(element.id[0])]+1
    return numberWinner

def processingNumberWinner():
    filesArray = fetchingResultsToArray('results/v2/coEvolution/beforeSwap')
    return auxProcessingNumberWinner(filesArray)

def processingNumberWinnerAfterSwap():
    filesArray = fetchingResultsToArray('results/v2/coEvolution/afterSwap')
    return auxProcessingNumberWinner(filesArray)

def processingResultsWinnerSwapped():
    filesArray = fetchingResultsToArray('results/v2/coEvolution/afterSwap')
    isWinnerSwappedArray = {}
    for i in range(0, len(filesArray)):
        try:
            isWinnerSwappedArray["results"+str(i)]
        except:
            isWinnerSwappedArray["results"+str(i)] = False
        if (filesArray[i]["origin"].values[0] != i):
            isWinnerSwappedArray["results"+str(i)] = True
    return isWinnerSwappedArray

def createResultsFolder():
    # Création des dossiers de résultats.
    rootDirectory = os.getcwd()
    try:
        pathlib.Path(os.path.join(rootDirectory, 'results/v1')).mkdir(parents=True, exist_ok=True)
        pathlib.Path(os.path.join(rootDirectory, 'results/v2/basic')).mkdir(parents=True, exist_ok=True)
        pathlib.Path(os.path.join(rootDirectory, 'results/v2/coEvolution/beforeSwap')).mkdir(parents=True, exist_ok=True)
        pathlib.Path(os.path.join(rootDirectory, 'results/v2/coEvolution/afterSwap')).mkdir(parents=True, exist_ok=True)
        pathlib.Path(os.path.join(rootDirectory, 'results/v2/coEvolution/json')).mkdir(parents=True, exist_ok=True)
    except OSError as error:
        print(error)

def computeNeuralNetworkSimulation(data):
    ## Préparation et traitement des données pour le réseau de neurones
    X, Y = dataUtility.splitAndEncode(data)
    xTrain, xTest, yTrain, yTest = dataUtility.splitDataToTrainAndTest(X, Y)
    xTrain, xTest = dataUtility.transformData(xTrain, xTest)

    ## Training
    neuralNetwork.train(xTrain, yTrain, 32, 250)

    ## Test
    yPred = neuralNetwork.predict(xTest)

    ## Matrice de confusion
    cm = neuralNetwork.createConfusionMatric(yTest, yPred)

    print("\n\n<Confusion Matrix> \n")
    """
        Notes : La matrice de confusion nous permet d'évaluaer la qualitée obtenue lors de la classification de notre 
        set de données.
        On obtient une matrice qui classe la valeur obtenue (VO) en fonction de la valeur attendue (VA).

            VO1     VO2     V03
        VA1  x       0       0  
        VA2  0       x       2  
        VA3  0       0       x  

        Donc peut voir le nombre de donnée bien prédite sur la diagonale, et les erreurs obtenue se situeront 
        à l'extérieure de la diagonale : Par exemple ici, On a obtenu deux fois V03, alors qu'on attendait VA2.

        En binaire (Deux valeurs attendues possibles) on peut donc traduire une matrice des manières suivantes : 

        Vrai Négatif    Faux Positif
        Faux Négatif    Vrai Positif

        Vrai Positif    Faux Négatif
        Faux Positif    Vrai Négatif
    """
    print (cm)

# Création des dossiers de résultats.
createResultsFolder()

# Entrainement du réseau de neurones.
startTotal = time.time()
computeNeuralNetworkSimulation(data)
neuralNetworkTime = time.time() - startTotal
# Lancement de l'algorithme évolutionnaire.
startEA = time.time()

"""
Explication des paramètres pour cette fonction : 
    - 1 : fichier de données à importer ;
    - 2 : Nombre de simulations (défaut -> 10) ;
    - 3 : Nombre de générations (défaut -> 500) ;
    - 4 : Nombre de paires de géniteurs lors de la sélection (défaut -> 350) ;
    - 5 : Probabilité de croisement (défaut -> 0.8) ;
    - 6 : Probabilité de mutation sur individu (défaut -> 0.8) ;
    - 7 : Probabilité de mutation sur caractéristique (défaut -> 0.5) ;
"""
# Lancement de l'algorithme évolutionnaire V1.
computeEvolutionaryAlgorithmSimulationV1(data, 10, 500, 350, 0.8, 0.8, 0.5);

"""
Explication des paramètres pour cette fonction : 
    - 2 : Nombre de simulations (défaut -> 10) ;
    - 3 : Nombre de générations (défaut -> 500) ;
    - 4 : Nombre de paires de géniteurs lors de la sélection (défaut -> 350) ;
    - 5 : Probabilité de croisement (défaut -> 0.8) ;
    - 6 : Probabilité de mutation sur individu (défaut -> 0.8) ;
    - 7 : Probabilité de mutation sur caractéristique (défaut -> 0.5) ;
"""
# Lancement de l'algorithme évolutionnaire V2.
computeEvolutionaryAlgorithmSimulationV2(10, 500, 350, 0.8, 0.8, 0.5);

"""
Explication des paramètres pour cette fonction : 
    - 2 : Nombre d'ilots (défaut -> 100) ;
    - 3 : Nombre de générations (défaut -> 500) ;
    - 4 : Nombre de paires de géniteurs lors de la sélection (défaut -> 350) ;
    - 5 : Probabilité de croisement (défaut -> 0.8) ;
    - 6 : Probabilité de mutation sur individu (défaut -> 0.8) ;
    - 7 : Probabilité de mutation sur caractéristique (défaut -> 0.5) ;
"""
# Lancement de l'algorithme évolutionnaire V2 avec CoEvolution.
computeEvolutionaryAlgorithmSimulationCoEvolution(10, 500, 350, 0.8, 0.8, 0.5)

evolutionaryAlgorithmTime = time.time() - startEA
totalTime = time.time() - startTotal

print("\n\n\nNeural network time : %s seconds." % neuralNetworkTime)
print("Evolutionary algorithm time : %s seconds." % evolutionaryAlgorithmTime)
print("Total time : %s seconds." % totalTime)